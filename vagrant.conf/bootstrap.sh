#!/usr/bin/env bash
parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

Update() {
    echo "-- Update packages --"
    sudo apt-get update
}

eval $(parse_yaml /vagrant/vagrant.conf/vagrant.yml "config_");

Update
sudo locale-gen pl_PL.UTF-8

echo "-- Prepare configuration for MySQL --"
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

echo "-- Install tools and helpers --"
export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -y software-properties-common python-software-properties mc git phpunit curl npm pv

echo "-- Install PPA's --"
sudo add-apt-repository ppa:ondrej/php
Update
apt-get install -y mysql-server

echo "-- Install packages (apache2, php7, phpmyadmin) --"
add-apt-repository -y ppa:ondrej/php && sudo apt-get update
apt-get install -y php7.1-cli php7.1-fpm php7.1-mysql php7.1-curl php-memcached php7.1-dev php7.1-mcrypt php7.1-sqlite3 php7.1-mbstring libapache2-mod-php7.1
apt-cache search php7.1


if ! [ -L /var/www/page ]; then
  rm -rf /var/www/page
  ln -fs /vagrant /var/www/page
fi

echo "-- Configure PHP & apache2 --"
cp /vagrant/vagrant.conf/page.conf /etc/apache2/sites-available/page.conf
cp /vagrant/vagrant.conf/page_ssl.conf /etc/apache2/sites-available/page_ssl.conf
a2ensite page.conf
a2ensite page_ssl.conf
a2enmod ssl
a2enmod rewrite

sed -i 's/;date.timezone =/date.timezone = Europe\/Warsaw/g' /etc/php5/apache2/php.ini
sed -i 's/;date.timezone =/date.timezone = Europe\/Warsaw/g' /etc/php5/cli/php.ini

sed -i 's/127.0.0.1/127.0.0.1 ecom.vagrant/g' /etc/hosts

echo "-- restart --"
a2dismod php5
a2enmod php7.1
service apache2 restart
service php7.1-fpm restart


echo "-- Setup databases --"
mysql -h 127.0.0.1 -u root -proot  -t -e "CREATE DATABASE IF NOT EXISTS $config_database_name; GRANT ALL ON $config_database_name.* to '$config_database_user'@'127.0.0.1' identified by '$config_database_password'; GRANT ALL ON $config_database_name.* to '$config_database_user'@'$config_database_ip' identified by '$config_database_password';"

echo "-- copy xdebug.ini --"
less /vagrant/vagrant.conf/xdebug.ini >> /etc/php5/mods-available/xdebug.ini

echo "-- copy virtual hosts --"
ln -s /var/www/page /home/vagrant/www


echo "-- copy bash_aliases --"
cp /vagrant/vagrant.conf/.bash_aliases /home/vagrant/.bash_aliases

# variable to change logs and cache folder on developers machines
echo "VM_DEV_MACHINE=1" >> /etc/environment


echo "-- Setup symfony --"

 mkdir -p /usr/local/bin
 curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
 chmod a+x /usr/local/bin/symfony
